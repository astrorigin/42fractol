/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rgb.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/29 16:22:07 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/03 17:59:39 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	rgb(int r, int g, int b)
{
	int	clr;

	clr = 0;
	clr |= ((unsigned char) r) << 16;
	clr |= ((unsigned char) g) << 8;
	clr |= ((unsigned char) b);
	return (clr);
}

void	rgb_rev(int i, uint8_t *r, uint8_t *g, uint8_t *b)
{
	*r = i >> 16;
	i -= *r << 16;
	*g = i >> 8;
	i -= *g << 8;
	*b = i;
}
