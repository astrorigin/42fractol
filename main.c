/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/28 20:08:17 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/04 18:13:05 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static int	init_2(t_parms *p)
{
	p->co = mlx_init();
	if (!p->co)
		return (error("mlx_init", "boo boo"));
	p->win = mlx_new_window(p->co, WX, WY, W_TITLE);
	if (!p->win)
		return (error("mlx_new_window", "failed"));
	return (init_hooks(p));
}

static int	init(t_parms *p, int argc, char *argv[])
{
	if (*argv[1] == 'M')
	{
		p->fractype = 0;
		if (!init_m(p, argc, argv))
			return (usage());
	}
	else if (*argv[1] == 'J')
	{
		p->fractype = 1;
		if (!init_j(p, argc, argv))
			return (usage());
	}
	else if (*argv[1] == 'K')
	{
		p->fractype = 2;
		if (!init_k(p, argc, argv))
			return (usage());
	}
	else
		return (usage());
	return (init_2(p));
}

int	main(int argc, char *argv[])
{
	t_parms	p;

	if (argc < 2)
		return (usage());
	ft_memset(&p, 0, sizeof(t_parms));
	if (!init(&p, argc, argv))
		return (0);
	redraw(&p);
	mlx_loop(p.co);
	return (0);
}
