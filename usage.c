/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/03 12:06:38 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/03 17:32:41 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	usage(void)
{
	const char	usage[] = "usage: "
		"fractol FRACTYPE [MAX_ITER] [Creal,Cimg] [N]\n\n"
		" FRACTYPE  = M(andelbrot), J(ulia), K(multi-julia)\n\n"
		" MAX_ITER  = iterations maximum (min. 1, default 50)\n"
		"             key_N => MAX_ITER + 1\n"
		"             key_M => MAX_ITER - 1\n\n"
		" Options Julia:\n"
		"  <Creal>,<Cimg>  = value of C\n"
		"                    key_H => Creal + 0.01\n"
		"                    key_J => Creal - 0.01\n"
		"                    key_K => Cimg + 0.005\n"
		"                    key_L => Cimg - 0.005\n\n"
		" Options multi-julia:\n"
		"  <N> = value of N (min. 1, default 2)\n"
		"        key_U => N + 1\n"
		"        key_I => N - 1\n\n"
		" Type 'x' to take a picture, and 'q' to quit.\n\n"
		"This is free software: you are free to change and redistribute it.\n"
		"There is NO WARRANTY, to the extent permitted by law.\n";

	ft_putstr(usage, 1);
	return (0);
}
