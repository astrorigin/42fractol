/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/03 10:34:27 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/03 20:43:15 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	init_hooks(t_parms *p)
{
	if (!mlx_hook(p->win, ON_DESTROY, 0, &on_destroy, p))
		return (error("mlx_hook", "on_destroy"));
	if (!mlx_key_hook(p->win, &key_pressed, p))
		return (error("mlx_key_hook", "key_pressed"));
	if (!mlx_mouse_hook(p->win, &mouse_event, p))
		return (error("mlx_mouse_hook", "mouse_event"));
	return (1);
}

int	init_m(t_parms *p, int argc, char *argv[])
{
	int	maxiter;

	if (argc < 3)
		p->maxiter = MAXITER;
	else
	{
		if (!ft_atoi(argv[2], &maxiter) || maxiter < 1)
			return (0);
		p->maxiter = maxiter;
	}
	p->zoom = 1;
	return (1);
}

int	init_j(t_parms *p, int argc, char *argv[])
{
	char	*s;

	if (!init_m(p, argc, argv))
		return (0);
	if (argc < 4)
	{
		p->creal = J_CREAL;
		p->cimg = J_CIMG;
	}
	else
	{
		s = ft_atod(argv[3], &p->creal);
		if (!s || *s != ',')
			return (0);
		if (!ft_atod(++s, &p->cimg))
			return (0);
	}
	p->c = p->creal - p->cimg * _Complex_I;
	return (1);
}

int	init_k(t_parms *p, int argc, char *argv[])
{
	if (!init_j(p, argc, argv))
		return (0);
	if (argc < 5)
		p->n = 2;
	else
	{
		if (!ft_atoi(argv[4], &p->n) || p->n < 1)
			return (0);
	}
	return (1);
}
