# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/12/28 19:58:25 by pmarquis          #+#    #+#              #
#    Updated: 2023/01/04 16:16:20 by pmarquis         ###   lausanne.ch        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re mrproper

# options

OPT_DEBUG ?= 0
export OPT_DEBUG

OPT_LINUX ?= 0
export OPT_LINUX

# end options

LIBFTDIR = libft

ifeq ($(OPT_LINUX),1)
MLXDIR = minilibx-linux
else
MLXDIR = minilibx_opengl
endif

NAME = fractol

INC = fractol.h $(LIBFTDIR)/libft.h $(LIBFTDIR)/printf/ft_printf.h
SRC = bmp.c error.c init.c julia.c key_pressed.c \
	  main.c mandelbrot.c mlx_pixel.c mouse_event.c multijulia.c \
	  on_destroy.c redraw.c rgb.c usage.c
OBJ = $(patsubst %.c,%.o,$(SRC))

CC = gcc

ifeq ($(OPT_DEBUG),1)
CFLAGS = -Wall -Wextra -g -fsanitize=address
else
CFLAGS = -Wall -Wextra -Werror -O3
endif

DEFINES =
ifeq ($(OPT_DEBUG),0)
DEFINES += -DNDEBUG
endif
ifeq ($(OPT_LINUX),1)
DEFINES += -DLINUX
endif

INCLUDES = -I$(LIBFTDIR) -I$(LIBFTDIR)/printf -I$(MLXDIR)

ARCHIVES = $(LIBFTDIR)/printf/libftprintf.a $(LIBFTDIR)/libft.a \
		   $(MLXDIR)/libmlx.a

all: $(NAME)

.c.o:
	$(CC) -c $(CFLAGS) $(DEFINES) $(INCLUDES) -o $@ $<

ifeq ($(OPT_LINUX),1)
$(NAME): $(OBJ) $(ARCHIVES)
	$(CC) $(CFLAGS) $^ -lXext -lX11 -lm -o $@
else
$(NAME): $(OBJ) $(ARCHIVES)
	$(CC) $(CFLAGS) $^ -framework OpenGL -framework AppKit -o $@
endif

$(LIBFTDIR)/libft.a:
	$(MAKE) -C $(LIBFTDIR)

$(LIBFTDIR)/printf/libftprintf.a:
	$(MAKE) -C $(LIBFTDIR)/printf

$(MLXDIR)/libmlx.a:
	$(MAKE) -C $(MLXDIR)

clean:
	rm -f *.o
	$(MAKE) -C $(LIBFTDIR) $@

fclean: clean
	rm -f $(NAME)
	$(MAKE) -C $(LIBFTDIR) $@
	$(MAKE) -C $(MLXDIR) clean

re: fclean all

mrproper: fclean
	rm -f cscope.* tags
	rm -f *.bmp
	$(MAKE) -C $(LIBFTDIR) $@

### deps
$(OBJ): $(INC)
