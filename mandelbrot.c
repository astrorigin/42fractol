/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/29 16:41:37 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/03 18:13:26 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static inline int	_color(double _Complex c, double _Complex t)
{
	const int	i = (int)(cabs(t) / cabs(c));

	return (rgb(i % 4 * 63, i % 8 * 31, i % 16 * 15));
}

static int	_mandle2(t_parms *p, double _Complex c, double _Complex t, int cnt)
{
	if (cabs(t) > 4)
		return (_color(c, t));
	if (cnt == p->maxiter)
		return (rgb(255 * (cabs((t * t)) / cabs((t - c) * c)), 0, 0));
	return (_mandle2(p, c, cpow(t, 2) + c, cnt + 1));
}

static void	_mandle(t_parms *p, int x, int y)
{
	double	ix;
	double	iy;
	int		clr;

	ix = (-2.0 + (p->offsetx * 0.5) + (x * (4.0 / WX))) / p->zoom;
	iy = (-1.0 + (p->offsety * 0.25) + (y * (2.0 / WY))) / p->zoom;
	clr = _mandle2(p, ix + iy * _Complex_I, 0, 0);
	mlx_pixel(p, x, y, clr);
}

void	mandelbrot(t_parms *p)
{
	int	i;
	int	j;

	ft_printf("mandelbrot: max_iter = %d, zoom = %llu\n", p->maxiter, p->zoom);
	i = 0;
	while (i < WX)
	{
		j = 0;
		while (j < WY)
		{
			_mandle(p, i, j);
			++j;
		}
		++i;
	}
}

void	mandelbrot_bmp(t_parms *p, int fd)
{
	int		x;
	int		y;
	double	ix;
	double	iy;
	t_pixel	pix;

	y = 0;
	while (y < WY)
	{
		x = 0;
		while (x < WX)
		{
			ix = (-2.0 + (p->offsetx * 0.5) + (x * (4.0 / WX))) / p->zoom;
			iy = (-1.0 + (p->offsety * 0.25) + (y * (2.0 / WY))) / p->zoom;
			rgb_rev(_mandle2(p, ix + iy * _Complex_I, 0, 0),
				&pix.r, &pix.g, &pix.b);
			write(fd, &pix, 3);
			++x;
		}
		++y;
	}
}
