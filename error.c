/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/28 20:31:17 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/03 12:06:51 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	error(const char *title, const char *msg)
{
	ft_dprintf(2, "error: %s: %s\n", title, msg);
	return (0);
}
