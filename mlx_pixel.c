/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_pixel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/31 18:10:01 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/03 12:10:44 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	mlx_pixel(t_parms *p, int x, int y, int clr)
{
	int	i;

	i = mlx_pixel_put(p->co, p->win, x, y, clr);
	return (i);
}
