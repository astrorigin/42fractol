/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/28 20:09:11 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/04 15:50:18 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <libft.h>
# include <ft_printf.h>
# include <mlx.h>

# ifdef LINUX
#  include <X11/X.h>
# endif

# include <stdint.h>
# include <complex.h>
# include <errno.h>
# include <math.h>
# include <string.h>

# ifndef NDEBUG
#  include <assert.h>
#  include <stdio.h>
# endif

# define WX		400
# define WY		200
# define W_TITLE	"fractol"
# define OUTFILE	"outfile"

# define MAXITER	50
# define J_CREAL	-0.4
# define J_CIMG		0.6

# ifdef LINUX
#  define MOUSE_CLICK_LEFT		1
#  define MOUSE_CLICK_RIGHT		3
#  define KEYCODE_ARROW_DOWN	65364
#  define KEYCODE_ARROW_LEFT	65361
#  define KEYCODE_ARROW_RIGHT	65363
#  define KEYCODE_ARROW_UP	65362
#  define KEYCODE_ESC	65307
#  define KEYCODE_H		104
#  define KEYCODE_I		105
#  define KEYCODE_J		106
#  define KEYCODE_K		107
#  define KEYCODE_L		108
#  define KEYCODE_M		109
#  define KEYCODE_N		110
#  define KEYCODE_Q		113
#  define KEYCODE_U		117
#  define KEYCODE_R		114
#  define KEYCODE_X		120
#  define ON_DESTROY	17
# else
#  define MOUSE_CLICK_LEFT		1
#  define MOUSE_CLICK_RIGHT		2
#  define KEYCODE_ARROW_DOWN	125
#  define KEYCODE_ARROW_LEFT	123
#  define KEYCODE_ARROW_RIGHT	124
#  define KEYCODE_ARROW_UP	126
#  define KEYCODE_ESC	53
#  define KEYCODE_H		4
#  define KEYCODE_I		34
#  define KEYCODE_J		38
#  define KEYCODE_K		40
#  define KEYCODE_L		37
#  define KEYCODE_M		46
#  define KEYCODE_N		45
#  define KEYCODE_Q		12
#  define KEYCODE_U		32
#  define KEYCODE_R		15
#  define KEYCODE_X		7
#  define ON_DESTROY	17
# endif

typedef struct s_parms
{
	void			*co;
	void			*win;
	int				fractype;
	int				maxiter;
	double			offsetx;
	double			offsety;
	t_ullong		zoom;
	double			creal;
	double			cimg;
	double _Complex	c;
	int				n;
}	t_parms;

typedef struct s_xy
{
	int	x;
	int	y;
}	t_xy;

typedef struct s_dxy
{
	double	x;
	double	y;
}	t_dxy;

typedef struct s_cxy
{
	double _Complex	x;
	double _Complex	y;
}	t_cxy;

typedef struct s_pixel
{
	uint8_t	b;
	uint8_t	g;
	uint8_t	r;
}	t_pixel;

int		error(const char *title, const char *msg);
int		usage(void);

int		init_j(t_parms *p, int argc, char *argv[]);
int		init_k(t_parms *p, int argc, char *argv[]);
int		init_m(t_parms *p, int argc, char *argv[]);
int		init_hooks(t_parms *p);

int		on_destroy(void *param);
int		key_pressed(int keycode, void *param);
int		mouse_event(int btn, int x, int y, void *param);

void	update_creal(t_parms *p, double x);
void	update_cimg(t_parms *p, double x);
void	reset(t_parms *p);
void	redraw(t_parms *p);

int		mlx_pixel(t_parms *p, int x, int y, int clr);
int		rgb(int r, int g, int b);
void	rgb_rev(int i, uint8_t *r, uint8_t *g, uint8_t *b);

void	mandelbrot(t_parms *p);
void	mandelbrot_bmp(t_parms *p, int fd);
void	julia(t_parms *p);
void	julia_bmp(t_parms *p, int fd);
void	multijulia(t_parms *p);
void	multijulia_bmp(t_parms *p, int fd);

int		take_bmp(t_parms *p);

#endif
