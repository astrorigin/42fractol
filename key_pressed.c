/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_pressed.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/28 22:11:28 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/04 15:46:55 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static int	key_pressed_4(int keycode, t_parms *p)
{
	if (keycode == KEYCODE_U)
	{
		p->n += 1;
		redraw(p);
	}
	else if (keycode == KEYCODE_I)
	{
		if (p->n > 1)
		{
			p->n -= 1;
			redraw(p);
		}
	}
	else if (keycode == KEYCODE_X)
		take_bmp(p);
	return (1);
}

static int	key_pressed_3(int keycode, t_parms *p)
{
	if (keycode == KEYCODE_H)
	{
		update_creal(p, 0.01);
		redraw(p);
	}
	else if (keycode == KEYCODE_J)
	{
		update_creal(p, -0.01);
		redraw(p);
	}
	else if (keycode == KEYCODE_K)
	{
		update_cimg(p, 0.005);
		redraw(p);
	}
	else if (keycode == KEYCODE_L)
	{
		update_cimg(p, -0.005);
		redraw(p);
	}
	else
		return (key_pressed_4(keycode, p));
	return (1);
}

static int	key_pressed_2(int keycode, t_parms *p)
{
	if (keycode == KEYCODE_ARROW_UP)
	{
		p->offsety -= 1;
		redraw(p);
	}
	else if (keycode == KEYCODE_ARROW_DOWN)
	{
		p->offsety += 1;
		redraw(p);
	}
	else if (keycode == KEYCODE_ARROW_LEFT)
	{
		p->offsetx -= 1;
		redraw(p);
	}
	else if (keycode == KEYCODE_ARROW_RIGHT)
	{
		p->offsetx += 1;
		redraw(p);
	}
	else if (keycode == KEYCODE_R)
		reset(p);
	else
		return (key_pressed_3(keycode, p));
	return (1);
}

int	key_pressed(int keycode, void *param)
{
	char	buf[12];
	t_parms	*p;

	p = (t_parms *) param;
	ft_itoa(keycode, buf);
	ft_printf("key_pressed: param=%p, code=%s\n", param, buf);
	if (keycode == KEYCODE_ESC || keycode == KEYCODE_Q)
		on_destroy(p);
	if (keycode == KEYCODE_N)
	{
		p->maxiter += 1;
		redraw(p);
	}
	else if (keycode == KEYCODE_M)
	{
		if (p->maxiter > 1)
		{
			p->maxiter -= 1;
			redraw(p);
		}
	}
	else
		return (key_pressed_2(keycode, p));
	return (1);
}
