/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_event.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/29 12:29:41 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/04 15:50:40 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	_offset(t_parms *p, int x, int y)
{
	p->offsetx += (x * 1.0 - WX / 2) / WX * 6;
	p->offsety += (y * 1.0 - WY / 2) / WY * 6;
}

static void	_zoomin(t_parms *p)
{
	p->zoom *= 2;
	p->offsetx *= 2;
	p->offsety *= 2;
}

static void	_zoomout(t_parms *p)
{
	p->zoom /= 2;
	p->offsetx /= 2;
	p->offsety /= 2;
}

int	mouse_event(int btn, int x, int y, void *param)
{
	t_parms	*p;

	ft_printf("mouse_event: btn=%d, x=%d, y=%d\n", btn, x, y);
	p = (t_parms *) param;
	if (btn == MOUSE_CLICK_LEFT && y >= 0)
	{
		_offset(p, x, y);
		_zoomin(p);
		redraw(p);
	}
	else if (btn == MOUSE_CLICK_RIGHT && y >= 0)
	{
		if (p->zoom >= 2)
		{
			_offset(p, x, y);
			_zoomout(p);
			redraw(p);
		}
	}
	return (0);
}
