/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bmp.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/03 14:06:24 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/03 18:11:42 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

typedef struct s_bmp_header
{
	char		bitmap_signature_bytes[2];
	uint32_t	size_of_bitmap_file;
	uint32_t	reserved_bytes;
	uint32_t	pixel_data_offset;
}	t_bmp_header;

typedef struct s_bmp_info_header
{
	uint32_t	size_of_this_header;
	int32_t		width;
	int32_t		height;
	uint16_t	number_of_color_planes;
	uint16_t	color_depth;
	uint32_t	compression_method;
	uint32_t	raw_bitmap_data_size;
	int32_t		horizontal_resolution;
	int32_t		vertical_resolution;
	uint32_t	color_table_entries;
	uint32_t	important_colors;
}	t_bmp_info_header;

static void	_init_header(t_bmp_header *hdr)
{
	hdr->bitmap_signature_bytes[0] = 'B';
	hdr->bitmap_signature_bytes[1] = 'M';
	hdr->size_of_bitmap_file = 54 + (WX * WY * 3);
	hdr->reserved_bytes = 0;
	hdr->pixel_data_offset = 54;
}

static void	_write_header(t_bmp_header *hdr, int fd)
{
	write(fd, &hdr->bitmap_signature_bytes, sizeof(char) * 2);
	write(fd, &hdr->size_of_bitmap_file, sizeof(uint32_t));
	write(fd, &hdr->reserved_bytes, sizeof(uint32_t));
	write(fd, &hdr->pixel_data_offset, sizeof(uint32_t));
}

static void	_init_info(t_bmp_info_header *hdr)
{
	ft_memset(hdr, 0, sizeof(t_bmp_info_header));
	hdr->size_of_this_header = 40;
	hdr->width = WX;
	hdr->height = -(WY);
	hdr->number_of_color_planes = 1;
	hdr->color_depth = 24;
	hdr->horizontal_resolution = 3780;
	hdr->vertical_resolution = 3780;
}

static char	*_tempnam(char *buf, int *i)
{
	char	bint[32];

	while (1)
	{
		ft_strcpy(buf, OUTFILE);
		ft_strcatchr(buf, '_', 1);
		ft_itoa(*i, bint);
		ft_strcat(buf, bint);
		ft_strcat(buf, ".bmp");
		if (ft_file_exists(buf))
			*i += 1;
		else
			break ;
	}
	return (buf);
}

int	take_bmp(t_parms *p)
{
	static int			i;
	char				fnam[512];
	int					fd;
	t_bmp_header		hdr;
	t_bmp_info_header	ihdr;

	_init_header(&hdr);
	_init_info(&ihdr);
	fd = open(_tempnam(fnam, &i), O_CREAT | O_WRONLY, 0644);
	if (fd < 0)
		return (error(fnam, strerror(errno)));
	_write_header(&hdr, fd);
	write(fd, &ihdr, sizeof(t_bmp_info_header));
	if (p->fractype == 0)
		mandelbrot_bmp(p, fd);
	else if (p->fractype == 1)
		julia_bmp(p, fd);
	else if (p->fractype == 2)
		multijulia_bmp(p, fd);
	else
		assert(0);
	close(fd);
	return (ft_printf("image saved as:  %s\n", fnam));
}
