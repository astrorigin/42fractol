/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   on_destroy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/28 23:26:17 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/04 15:36:47 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

#ifdef LINUX

int	on_destroy(void *param)
{
	t_parms	*p;

	p = (t_parms *) param;
	mlx_destroy_window(p->co, p->win);
	mlx_destroy_display(p->co);
	ft_putstr("bye!\n", 1);
	exit(0);
	return (0);
}

#else

int	on_destroy(void *param)
{
	t_parms	*p;

	p = (t_parms *) param;
	mlx_destroy_window(p->co, p->win);
	ft_putstr("bye!\n", 1);
	exit(0);
	return (0);
}

#endif
