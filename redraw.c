/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redraw.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/01 12:05:22 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/04 15:37:25 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	update_creal(t_parms *p, double x)
{
	p->creal += x;
	p->c = p->creal - p->cimg * _Complex_I;
}

void	update_cimg(t_parms *p, double x)
{
	p->cimg += x;
	p->c = p->creal - p->cimg * _Complex_I;
}

void	reset(t_parms *p)
{
	p->maxiter = MAXITER;
	p->offsetx = 0;
	p->offsety = 0;
	p->zoom = 1;
	redraw(p);
}

void	redraw(t_parms *p)
{
	if (p->fractype == 0)
		mandelbrot(p);
	else if (p->fractype == 1)
		julia(p);
	else if (p->fractype == 2)
		multijulia(p);
	else
		assert(0);
}
