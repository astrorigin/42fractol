/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/01 12:13:32 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/03 18:01:22 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static inline int	_color(int i)
{
	return (rgb(i % 4 * 63, i % 8 * 31, i % 16 * 15));
}

static int	_juliapoint(t_parms *p, int depth, double _Complex z)
{
	if (cabs(z) > 4)
		return (_color(depth));
	if (depth <= p->maxiter / 8)
		return (rgb(255 * (cabs((z * z)) / cabs((z - p->c) * p->c)), 0, 0));
	return (_juliapoint(p, depth - 1, cpow(z, 2) + p->c));
}

static void	_julia(t_parms *p, int x, int y)
{
	int		clr;
	double	ix;
	double	iy;

	ix = (-2.0 + (p->offsetx * 0.5) + (x * (4.0 / WX))) / p->zoom;
	iy = (-1.0 + (p->offsety * 0.25) + (y * (2.0 / WY))) / p->zoom;
	clr = _juliapoint(p, p->maxiter, ix + iy * _Complex_I);
	mlx_pixel(p, x, y, clr);
}

void	julia(t_parms *p)
{
	int	i;
	int	j;

	ft_printf("julia: max_iter = %d, creal = %f, cimg = %f, zoom = %llu\n",
		p->maxiter, p->creal, p->cimg, p->zoom);
	i = 0;
	while (i < WX)
	{
		j = 0;
		while (j < WY)
		{
			_julia(p, i, j);
			++j;
		}
		++i;
	}
}

void	julia_bmp(t_parms *p, int fd)
{
	int		x;
	int		y;
	double	ix;
	double	iy;
	t_pixel	pix;

	y = 0;
	while (y < WY)
	{
		x = 0;
		while (x < WX)
		{
			ix = (-2.0 + (p->offsetx * 0.5) + (x * (4.0 / WX))) / p->zoom;
			iy = (-1.0 + (p->offsety * 0.25) + (y * (2.0 / WY))) / p->zoom;
			rgb_rev(_juliapoint(p, p->maxiter, ix + iy * _Complex_I),
				&pix.r, &pix.g, &pix.b);
			write(fd, &pix, 3);
			++x;
		}
		++y;
	}
}
